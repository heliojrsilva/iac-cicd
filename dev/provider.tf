# Authenticate with GCP Service account
provider "google" {
  credentials = file("CREDENTIAL")

  project = "iac-cicd-420122"
  region  = "us-central1"
  zone    = "us-central1-c"
}